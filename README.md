# Git + Github

## Exercise 1

What does the following command do?

    1. git clone remote-repo-url
    2. git init
    3. git status
    4. git checkout -b branch-name
    5. git checkout branch-name
    6. git add .
    7. git add file-name
    8. git commit commit-message
    9. git push
    10. git pull
    11. git merge branch-name
    12. gitex commit (this isn't git command of course but in our case, what will happen if you run this?)
    13. git mergetool

**Bonus for Exercise 1**

Considering the below screenshot:

- What will you see in section marked ***`1`*** ?
- What git command will be running when button marked:
  - ***`2`*** is clicked?
  - ***`3`*** is clicked?
  - ***`4`*** is clicked?
  - ***`5`*** is clicked?
- What will you have in section marked `6`?
- What will you have in `lower left section`?

![Gitex Screenshot](https://bitbucket.org/ZelalemW/git-exercises/raw/master/images/gitex-ui.PNG)

## Exercise 2

If you want to do the following actions, what command will you be running

    1. pull down repo for the first time from Github to your machine - so you will have a local copy of it along with its version history

    2. check status of what files are added, modified or deleted before making commits

    3. stage one-file among those added, modified or deleted
    4. stage all files

    5. merge (i.e integrate) other developers work that was pushed into master after you get the latest last time

    6. if there are merge conflicts and want to resolve it and want to open default mergetool you have setup (in our case its KDiff3)

    7. make your staged changes become permanent history of Git version control system

    8. instead of cloning an existing project from Github, can you convert a project you have into git version controlled? If the answer is yes, what command will you be running?

## Exercie 3

1. Go to your Github and create a new repository.

    - repo name: `my-todos`
    - make it private
    - add readme file.

2. Clone your newly created repo

3. Open your project in your favorite IDE, Visual Studio Code, or Visual Studio for instance. Hopefully, you have Visual Studio Code installed (its light weight and works really well on Windows, Linux as well as Mac)

4. Check if there is any change made. We know there is none, you just cloned it anyway but its excercise so do it so you know there is none.

5. Open your README.md file and change the title to `Todos` &save it.

6. Now go to your command prompt and check status

7. I am sure you see changes, the modified file listed there. Now, when you answer, the following, please answer them independetly as if that was the only question asked for #7

        a) Where exactly is the change at this point?

        b) Is it already part of Git's version control history?

        c) Is it available only locally or will you find that change on Github too?

        d) If you attempt to commit, what do you think will happen?

        e) How about if you attempt to push it to Github?

8. By now, you realized that your change is actually not ready for commit and you have to do something about it. Please proceed and stage the file.

9. Check status, I know, I know, just to make sure. Its our job to be detail oriented, make less assumptions and instead rely on verification. What do you see? The modiefied file being tracked or not?

10. Now go make your change become permanent record of Git's history. Don't forget to give meaningful message

11. Do you think your change made it to Github by now or perhaps not yet? Keep your answer but then go to Github and verify. What do you see

12. Push your change to remote (in this case, Github)

13. Go to Github and verify. What do you see for the `README.md` file's content? Did you see your change?

14. Now get the latest pretending you are working as a team member and hence other developers in your time might have pushed some change.

15. Now you just started a new feature work and that is to add few `Todo` items. Since you know, you `shouldn't` be working directly on `master` branch, you need to create a `new branch` with meaningful name, lets call it `add-todos`

16. Go to your file and please add the following.

        - revise Github session
        - watch movie
        - read 1 chapter in `blabla` book
        - call `blabla` friend
        - excercise

17. Add new file, call it, `Groceries.md` and add one or more list of groceries (Muzz, Birtukan, stuff like that) and save it.

18. Check status, I am guessing, Git it saying there are 2 files (1 modified, 1 added) but both not tracked (untracked). Is it?

19. Now be selective and stage Just `README.md` file only. 

20. Commit with meaningful message. Feel free to come up with your own message.

21. Stage newly created file in Q17.

22. Commit your change with meaningful message as well.

23. Push your commits to Github. Since you are working on local branch that don't exist in Github yet, `Git` will ask you to set upstream origin, please run the suggested command. That will make your newly created branch have a twin brother/sister on Github :)

24. Go to Github and verify. Did you see all your changes `with 2 commits` as part of your new branch?

25. Switch into `master` branch

26. Pretend other developers might have pushed changes since the last time you get the latest and hence, it will be advisable to get latest now.

27. Hmmm, take a look modification you made from your branch isn't showing now. But why? Does it mean, its lost?

28. Change the title for `README.md` file from `Todos` to `My Todos`. Add also the following and save it

        - excercise
        - making juice
        - walk for 45 mins

    Note that: From Q25, we are mimicing what other developers might have done while we were busy with our other tasks.

29. Check status
30. Stage the file for commit
31. Commit it with short and precise message that speaks intent of the change made
32. Push it to Github
33. Go to Github and verify your change. But don't forget to switch between branches (`master` & `add-todos`) in Github.
34. Pull latest (even if you are the only developer at this point), develop the mindset.
35. Switch into your branch `add-todos`, since that is just your branch, you don't have to pull latest. What you have locally and whats on Github should be the same
36. Now `merge master branch` into your branch
37. Since we intentionally introduced changes on the same file, on the same lines, I am sure you will run into `merge conflicts` on one `README.md` file. Is it or all went well?
38. Open your merge tool to resolve merge conflicts. At this point, when the `KDiff3 tool` opens, it will show you `4 main sections`
    - Base
    - Local
    - Remote
    - Lower section (this is not a label of course.)

    a) Which section holds the change you made as part of your feature branch?

    b) Which one represents the code change since you branch out and change made by other developers (which in this particular instance you but on a master branch)

    c) And which one represents the code that your branch was based on just when you create the branch

    d) Now do the actual merging. At this point, you got to `chery pick` what to take and what to leave behind. I would say, take both sides except, leave `walk for 45 mins` and instead take `excercise`. Save and close KDiff. If there is another conflict on a different file. Do this until all issues on the file as well as on ALL files that have conflicts are resolved. In this case, you only have one file with conflict and i say, you are one lucky fella

39. You can now start using gitextensions tool you setup. Run gitex commit and it will popup. 
    - Stage the file
    - reset `.orig` file please. Just right click and reset it. Or if you prefer, run `git clean -f` on your command prompt. Either way, will knock out this unwanted file that `Git` created because of the conflict.
    - On lower right section of gitextentions, write a message `merge from master` and click on `commit & push` button
40. Go to Github and verify that your branch should now have the changes made by other developers too. 
41. Since you are done with this feature branch you have been working on, create a `Pull Request` in `Github` and `send it for code review`

42. Run `git log --oneline --graph --decorate --all` to see what the branching looks like at this point in time.

43. **Congratulations!**

    Believe it or not, you learn a lot! You have now a tool at your disposal to use in any projects.

**Happy Coding!**
